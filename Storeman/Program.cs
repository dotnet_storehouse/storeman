﻿using System.Drawing;
using Storebase.args;
using Storehouse;
using SHCL = Storehouse.StorehouseClientLibrary;
using Console = Colorful.Console;

namespace Storeman {
    internal static partial class Program {
        private static bool _pauseLoop;

        public static void Main(string[] args) {
            var hostname = new Argument<string>("hostname", "h").Fetch(args, "localhost");
            var port = new Argument<int>("port", "p").Fetch(args, 62822);
            var bucketCache = new Argument<string>("bucket-cache", "bc").Fetch(args, "buckets.cache");
            var certificatePath = new Argument<string>("cert-path", "cp").Fetch(args, "Storeman.pfx");

            SHCL.Initialize(new StorehouseSettings {
                BucketCacheFile = bucketCache,
                CertificatePath = certificatePath
            });
            SHCL.Connect(hostname, port);

            Console.WriteAscii("Storeman");

            while (SHCL.NetML.IsRunning) {
                Console.Write("[SSL]", Color.Green);
                if (SHCL.ActiveBucket != null)
                    Console.Write("[" + SHCL.ActiveBucket.Guid.ToString().Substring(0, 8) + "]", Color.Blue);

                Console.Write("> ");
                var input = Console.ReadLine();

                ExecuteCommand(input);
            }

            SHCL.SaveBuckets();
        }

        private static bool ExecuteCommand(string commandLine) {
            var ret = false;
            Commands.ForEach(cmd => ret |= cmd.Execute(commandLine));
            while (_pauseLoop) { }

            return ret;
        }
    }
}