﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.Linq;
using Storehouse.err;
using SHCL = Storehouse.StorehouseClientLibrary;
using Storeman.cmd;
using Console = Colorful.Console;

namespace Storeman {
    internal static partial class Program {
        private static readonly List<Command> Commands = new List<Command> {
            new Command("bucket", new List<Command> {
                new Command("list", args => {
                    Console.WriteLine("Available buckets: " + SHCL.Buckets.Count);
                    Console.WriteLine("Local buckets:");
                    SHCL.Buckets.ForEach(Console.WriteLine);
                    return true;
                }),
                new Command("list-remote", args => {
                    var lst = SHCL.GetRemoteStorageBuckets();
                    Console.WriteLine("Available buckets: " + lst.Count);
                    lst.ForEach(Console.WriteLine);

                    return true;
                }),
                new Command("save", args => {
                    SHCL.SaveBuckets();
                    return true;
                }),
                new Command("add", args => {
                    if (args.Length != 2) {
                        Console.WriteLine("Usage: bucket add <name> <path>");
                        Console.WriteLine("\tSpaces not supported");
                        return false;
                    }

                    try {
                        var b = SHCL.CreateAndAddBucket(args[0], args[1]);
                        Console.WriteLine("Added bucket " + b);
                        return true;
                    } catch (DuplicateStorageBucketException e1) {
                        Console.WriteLine(
                            "A bucket with this guid is already present. Just try again as the odds of this are negligible.",
                            Color.DarkRed);
                        return false;
                    }
                }),
                new Command("push", args => {
                    if (args.Length != 1 && SHCL.ActiveBucket == null) {
                        Console.WriteLine("Usage: bucket push <guid>");
                        Console.WriteLine(
                            "\tThe start of the GUID suffices, as long as it is enough to uniquely identify a bucket.");
                        return false;
                    }

                    var b = SHCL.ActiveBucket;
                    if (SHCL.ActiveBucket == null) {
                        try { b = SHCL.GetBucketByGuid(args[0]); } catch (StorageBucketNotFoundException e1) {
                            Console.WriteLine("There is no bucket with such a guid.", Color.DarkRed);
                        } catch (AmbiguousGuidException e2) {
                            Console.WriteLine("The given guid fragment is not unique.", Color.DarkRed);
                        }
                    }

                    Console.WriteLine("Pushing...");

                    try {
                        SHCL.PushBucket(b);
                        return true;
                    } catch (DuplicateStorageBucketException e1) {
                        Console.WriteLine(
                            "A bucket with this guid is already present on the remote." +
                            "If you are sure that this is a different one, try regenerating it's guid.",
                            Color.DarkRed);
                        return false;
                    }

                    return true;
                }),
                new Command("fetch", args => {
                    if (args.Length != 1) {
                        Console.WriteLine("Usage: bucket fetch <guid>");
                        Console.WriteLine(
                            "\tThe start of the GUID suffices, as long as it is enough to uniquely identify a bucket.");
                        return false;
                    }

                    Console.WriteLine("Fetching...");

                    try {
                        SHCL.FetchBucket(args[0]);
                        Console.WriteLine("Fetched bucket '" + SHCL.LastFetchedBucket + "'.");
                        return true;
                    } catch (StorageBucketNotFoundException e1) {
                        Console.WriteLine("There is no bucket with such a guid on the remote", Color.DarkRed);
                    } catch (AmbiguousGuidException e2) {
                        Console.WriteLine("The given guid fragment is not unique (on the remote).", Color.DarkRed);
                    }

                    return false;
                }),
                new Command("pull", args => {
                    if (args.Length < 1 || args.Length > 2) {
                        Console.WriteLine("Usage: bucket pull <guid> <local path>");
                        Console.WriteLine(
                            "\tThe start of the GUID suffices, as long as it is enough to uniquely identify a bucket.");
                        Console.WriteLine("\tIf the path is omitted, the path set on the remote will be used.");
                        return false;
                    }

                    try {
                        var b = SHCL.PullBucket(args[0]);
                        Console.WriteLine("Pulled bucket '" + b + "'.");
                        return true;
                    } catch (StorageBucketNotFoundException e1) {
                        Console.WriteLine("There is no bucket with such a guid on the remote", Color.DarkRed);
                    } catch (AmbiguousGuidException e2) {
                        Console.WriteLine("The given guid fragment is not unique (on the remote).", Color.DarkRed);
                    } catch (DuplicateStorageBucketException e1) {
                        Console.WriteLine("A bucket with this guid is already present.", Color.DarkRed);
                    }

                    return false;
                }),
                new Command("use", args => {
                    if (args.Length != 1) {
                        Console.WriteLine("Usage: bucket use <guid>");
                        Console.WriteLine(
                            "\tThe start of the GUID suffices, as long as it is enough to uniquely identify a bucket.");
                        return false;
                    }

                    try {
                        SHCL.SetActiveBucket(args[0]);
                        return true;
                    } catch (StorageBucketNotFoundException e1) {
                        Console.WriteLine("There is no bucket with such a guid.", Color.DarkRed);
                    } catch (AmbiguousGuidException e2) {
                        Console.WriteLine("The given guid fragment is not unique.", Color.DarkRed);
                    }

                    return false;
                }),
                new Command("watch-s", args => {
                    if (args.Length != 1 && SHCL.ActiveBucket == null) {
                        Console.WriteLine("Usage: bucket watch-s <guid>");
                        Console.WriteLine(
                            "\tThe start of the GUID suffices, as long as it is enough to uniquely identify a bucket.");
                        return false;
                    }

                    var b = SHCL.ActiveBucket;
                    if (SHCL.ActiveBucket == null) {
                        try { b = SHCL.GetBucketByGuid(args[0]); } catch (StorageBucketNotFoundException e1) {
                            Console.WriteLine("There is no bucket with such a guid.", Color.DarkRed);
                        } catch (AmbiguousGuidException e2) {
                            Console.WriteLine("The given guid fragment is not unique.", Color.DarkRed);
                        }
                    }

                    b.EnableFsw(false);

                    return true;
                }),
                new Command("change-s", args => {
                    if (args.Length != 1 && SHCL.ActiveBucket == null) {
                        Console.WriteLine("Usage: bucket change-s <guid>");
                        Console.WriteLine(
                            "\tThe start of the GUID suffices, as long as it is enough to uniquely identify a bucket.");
                        return false;
                    }

                    var b = SHCL.ActiveBucket;
                    if (SHCL.ActiveBucket == null) {
                        try { b = SHCL.GetBucketByGuid(args[0]); } catch (StorageBucketNotFoundException e1) {
                            Console.WriteLine("There is no bucket with such a guid.", Color.DarkRed);
                        } catch (AmbiguousGuidException e2) {
                            Console.WriteLine("The given guid fragment is not unique.", Color.DarkRed);
                        }
                    }

                    var change = SHCL.GetSimpleChanges(b);

                    if (change.Count > 0) {
                        var maxName = change.Max(diff => diff.FileName.Length) + 2;
                        var tLen = DateTime.Now.ToString(CultureInfo.CurrentCulture).Length;
                        var ne = "nonexistent".PadRight(tLen);

                        Console.WriteLine(" Name ".PadRight(maxName) + "|" + " Local change ".PadRight(tLen + 2) + "|" +
                                          " Remote change".PadRight(tLen));

                        foreach (var c in change) {
                            var loc = c.New.IsRemote ? c.Old : c.New;
                            var rem = c.New.IsRemote ? c.New : c.Old;

                            Console.Write((" " + c.FileName + " ").PadRight(maxName) + "|");
                            Console.Write(
                                " " + (loc != null
                                    ? (new DateTime(loc.ChangeTime)).ToString(CultureInfo.CurrentCulture)
                                    : ne) + " ",
                                c.New == loc ? Color.Lime : Color.Red);
                            Console.Write("|");
                            Console.Write(
                                " " + (rem != null
                                    ? (new DateTime(rem.ChangeTime)).ToString(CultureInfo.CurrentCulture)
                                    : ne) + " ",
                                c.New == rem ? Color.Lime : Color.Red);

                            Console.WriteLine();
                        }
                    } else { Console.WriteLine("No changes detected."); }

                    return true;
                }),
                new Command("upload", args => {
                    if (args.Length < (SHCL.ActiveBucket == null ? 2 : 1) || args.Length > 2) {
                        Console.WriteLine("Usage: bucket send-file <guid> <filename>");
                        Console.WriteLine("\t<guid> can be omitted when there is an active Bucket.");
                        return false;
                    }

                    var b = SHCL.ActiveBucket;
                    var filename = "";
                    if (SHCL.ActiveBucket == null) {
                        try {
                            b = SHCL.GetBucketByGuid(args[0]);
                            filename = args[1];
                        } catch (StorageBucketNotFoundException) {
                            Console.WriteLine("There is no bucket with such a guid.", Color.DarkRed);
                        } catch (AmbiguousGuidException) {
                            Console.WriteLine("The given guid fragment is not unique.", Color.DarkRed);
                        }
                    } else { filename = args[0]; }

                    try {
                        SHCL.SendFile(b, filename);
                        Console.WriteLine($"Uploading {filename}...");
                    } catch (Exception) { Console.WriteLine("An error occurred.", Color.DarkRed); }

                    return true;
                }),
                new Command("download", args => {
                    if (args.Length < (SHCL.ActiveBucket == null ? 2 : 1) || args.Length > 2) {
                        Console.WriteLine("Usage: bucket send-file <guid> <filename>");
                        Console.WriteLine("\t<guid> can be omitted when there is an active Bucket.");
                        return false;
                    }

                    var b = SHCL.ActiveBucket;
                    var filename = "";
                    if (SHCL.ActiveBucket == null) {
                        try {
                            b = SHCL.GetBucketByGuid(args[0]);
                            filename = args[1];
                        } catch (StorageBucketNotFoundException) {
                            Console.WriteLine("There is no bucket with such a guid.", Color.DarkRed);
                        } catch (AmbiguousGuidException) {
                            Console.WriteLine("The given guid fragment is not unique.", Color.DarkRed);
                        }
                    } else { filename = args[0]; }

                    try {
                        SHCL.DownloadFile(b, filename);
                        Console.WriteLine($"Downloading {filename}...");
                    } catch (Exception) { Console.WriteLine("An error occurred.", Color.DarkRed); }

                    return true;
                })
            }),
            new Command("exit", args => {
                SHCL.NetML.Stop();
                //_pauseLoop = true;
                return true;
            })
        };
    }
}