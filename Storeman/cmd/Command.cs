﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Storeman.cmd {
    public class Command {
        private readonly string _command;
        private readonly List<Command> _subCommands;
        private readonly Func<string[], bool> _action;
        
        public Command(string cmd, List<Command> cmds) {
            _command = cmd;
            _subCommands = cmds;
        }

        public Command(string cmd, Func<string[], bool> action) {
            _command = cmd;
            _action = action;
        }
        
        public bool Execute(string line) {
            var s = line.Split(' ');
            return s[0].Equals(_command, StringComparison.InvariantCultureIgnoreCase) && Execute(s);
        }

        private bool Execute(string[] args) {
            if (!args[0].Equals(_command, StringComparison.InvariantCultureIgnoreCase)) return false;

            var newArgs = args.Skip(1).ToArray();
            var ret = false;
            _subCommands?.ForEach(cmd => ret |= cmd.Execute(newArgs));
            var r1 = _action?.Invoke(newArgs);
            return ret || r1.HasValue && r1.Value;
        }
    }
}